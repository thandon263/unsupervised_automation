const puppeteer = require('puppeteer');


module.exports = async function hotelScrapper () {

  let destination = "san-diego-california";

  //Destination Codes
  //las-vegas-nevada||san-diego-california||san-francisco-california
  //los-angeles-california||denver-colorado||houston-texas
  //chicago-illinois||dallas-texas||inneapolis-minnesota
  //atlanta-georgia||washington-district-of-columbia||philadelphia-pennsylvania
  
    
    const browser = await puppeteer.launch({
      headless: false
    });
    const page = await browser.newPage()

    let url = `https://www.wyndhamhotels.com/hotels/${destination}`;

  try {

    await page.goto(url);
    await page.waitForSelector("#list-view");

    page.evaluate(_ => {
        window.scrollBy(0, 1500);
      });

    await page.waitFor(1000)

    page.evaluate(_ => {
        window.scrollBy(0, 1500);
      });

      await page.waitFor(1000)


      const sections = await page.$$(".propSummary");
    
    // // Print hotel data
    for ( let i = 0; i < sections.length; i++) {

        const section = sections[i];

        let price = "";

        const hotel = await section.$(".hotel-url");
        const hotel__details = await section.$(".hotel-tagline");
        const roomPrice = await section.$(".rate");
        const hotel__image = await section.$(".lazy-load");
        
        

        const hotelName = await page.evaluate( title => title.innerText, hotel);
        const hotelDetails = await page.evaluate( hotelDetails => hotelDetails.innerText, hotel__details);
        
        try {
            price  = await page.evaluate( price => price.innerText, roomPrice);
            
        } catch (e) {
            price = "unavailable";
        }
        
       const image = await page.evaluate( image => image.src, hotel__image);
        
        console.log("\n\nfffffffff\n\n");
        console.log("Title: ", hotelName);
        console.log("Description: ", hotelDetails);
        console.log("Price: ", price);
        console.log("Image: ", image);


    };

    
    await browser.close();

  } catch(e) {
    console.log("Error message ", e);
    await browser.close()
  };

}

