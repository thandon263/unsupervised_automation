const puppeteer = require('puppeteer');
const chromium = require("chromium");
const moment = require("moment");


module.exports = async function hotelScrapping(hotelCode, hotelName) {
  let url = "https://www.totalrewards.com/reserve/#hotel";
  let hotelURL = "https://www.caesars.com/";
  let destination = "Las Vegas";
  let selectItem = hotelName;
  let hotel = hotelCode;
  let check_in = "06/15/2018";
  let check_out = "06/18/2018";
  let room = 0;
  let list_number_adults = 0;

  const browser = await puppeteer.launch({
    executablePath: chromium.path,
    headless: true
  });
  const page = await browser.newPage();
  await page.setViewport({
    width: 1000,
    height: 900
  });

  try {

    await page.goto(url);
    await page.waitFor(1000);
    await page.select("#location", destination);
    await page.type('.date-from', `${check_in}`);
    await page.type('.date-to', `${check_out}`);
    await page.click( '#btnSearch');

    await page.waitForSelector('#div-securepur');
    await page.waitFor(5000);

    //let nights = await page.select(".select-area.jcf-unselectable.select-hotels-select.select-los_focus.jcf-drop-active", days);
    await page.click(`[code=${hotel}]`);
    await page.waitForSelector("section.box.discount-box");

    // add dates and then search for rooms
    await page.type("input.date-from.default", `${check_in}`);
    // write to the ${date-from} field 
    await page.type("input.date-to.hidecheckoutdate.widget_depart.default", `${check_out}`);
    // write to the ${date-to} field
    await page.click("#btnSearch");
    // Search the ${btn-search}

    await page.waitForSelector("section.box.discount-box");

    const sections = await page.$$("section.box.discount-box");
    console.log("Hotel: ", selectItem);
    console.log("Dates: -- ", check_in, " ", check_out);

    for ( let i = 0; i < sections.length; i++) {
        const section = sections[i];

        const room = await section.$(".room");
        const room__detail = await section.$(".rdescription");
        const room__image = await section.$(".richContent > img");
        // const old__price = await section.$(".def-old-price");
        const new__price = await section.$(".new-price");
        const roomName = await page.evaluate( room => room.innerText, room);
        const roomDetail = await page.evaluate( roomDetail => roomDetail.innerText, room__detail);
        // const oldPrice = await page.evaluate( oldPrice => oldPrice.innerText, old__price);
        const newPrice = await page.evaluate( newPrice => newPrice.innerText, new__price);
        const image = await page.evaluate( image => image.src, room__image);
        console.log("\n\n");
        console.log("Title: ", roomName);
        console.log("New Price: ", newPrice);
        console.log("Description: ", roomDetail);
        console.log("Image: ", image);
        // const name = await li.$eval("h2", h2 => h2.innerText);
        // console.log("name: ", name);
    };


    // await page.click(".btn-rates")

    // await page.click("#btnContinueBeforeVouchers");
    // await page.waitFor(5000);
    // await page.click("#btnContinueBeforeVouchers");

    // close browser session
    browser.close();

  } catch (e) {
    browser.close(); 
    console.log(e);
  };

  
};

