const puppeteer = require("puppeteer");
const chromium = require("chromium");

module.exports = async function flightScrapping() {
    let depCode = "LAX"; //LAX
    let arrCode = "LAS"; //LAS
    let depDate = "2018-08-01"; //YYYY-MM-DD
    let retDate = "2018-08-09"; //YYYY-MM-DD
    let paxCount = "2"; //1
    let passengers = "1";
    let roundtrip = false;
    let url = "https://www.united.com/ual/en/us/flight-search/book-a-flight";

    // Url for the flights
    let  unitedOneway = `https://www.united.com/ual/en/us/flight-search/book-a-flight/results/rev?f=${depCode}&t=${arrCode}&d=${depDate}&tt=1&sc=7&px=${passengers}&taxng=1&idx=1`
    let unitedReturn = `https://www.united.com/ual/en/us/flight-search/book-a-flight/results/rev?f=${depCode}&t=${arrCode}&d=${depDate}&r=${retDate}&sc=7,7&px=${passengers}&taxng=1&idx=1`


    const browser = await puppeteer.launch({
        executablePath: chromium.path,
        headless: false
    });
    
    const page = await browser.newPage();
    
    await page.setViewport({
        width: 1000,
        height: 900
    })
    
    try {

    await page.goto(unitedReturn)// ? roundtrip : await page.goto(unitedOneway)
    await page.waitFor(1000)
    // await page.type("#Trips_0__Origin", origin);
    // await page.type("#Trips_0__Destination", destination);
    // await page.select("#cboMiles", "50");
    // await page.select("#cboMiles2", "50");
    // await page.click(".modalCloseImg");
    // Logic on the search - round trip or one way.
    // if (!roundtrip) {
        // if the round trip is false
        // click on the { One way button }.
        // await page.click("#TripTypes_ow");
        // console.log("Round Trip: false");
    // }
    // Get dates and enter departure and return
    // Use the standard notation
    // await page.type("#Trips_0__DepartDate", depature);
    // await page.type("#Trips_0__ReturnDate", returning);
    // // Submit search
    // await page.click("#btn-search");
    // Get the load and wait for the selector to render
    // Read the page and get it working;
    // await page.waitForSelector("#flight-result-elements");
    await page.waitFor(5000);
    // Log the results of the flights
    // await page.waitForSelector(".flight-block-revised");
    // get list of results
    // const lists = await page.$$(".flight-block-revised");
    // console.log("Number of flights: ", lists.length);
    await page.waitForSelector
    // for ( let i = 0; i < lists.length; i++ ) {
    //     const list = lists[i];

        const departure = await page.evaluate(() => {
            const flight = document.querySelector(".flight-time.flight-time-depart").innerText
            return flight;
        });

        const arrival = await page.evaluate(() => {
            const flight = document.querySelector(".flight-time.flight-time-arrive").innerText
            return flight;
        });

        const price = await page.evaluate(() => {
            const flight = document.querySelector(".use-roundtrippricing").innerText
            return flight;
        });

        console.log("Departure: ", departure, "\nArrival: ", arrival);
        console.log("Prices: ", price);
    // }

    await page.screenshot({ path: "flights.png" });

    // await browser.close();

    } catch (e) {
        console.log("Error message ", e);
    };

};