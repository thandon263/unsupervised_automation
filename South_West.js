const puppeteer = require('puppeteer');

module.exports = async function flight_Scrapper() {
    
    const browser = await puppeteer.launch(
        {headless: false}
    )
    const page = await browser.newPage()

    let depCode = "LAX"; //LAX
    let arrCode = "LAS"; //LAS
    let depDate = "2018-08-01"; //YYYY-MM-DD
    let retDate = "2018-08-09"; //YYYY-MM-DD
    let paxCount = "2"; //1

    let urlReturn = `https://www.southwest.com/air/booking/select.html?originationAirportCode=${depCode}&destinationAirportCode=${arrCode}&returnAirportCode=&departureDate=${depDate}&departureTimeOfDay=ALL_DAY&returnDate=${retDate}&returnTimeOfDay=ALL_DAY&adultPassengersCount=${paxCount}&seniorPassengersCount=0&fareType=USD&passengerType=ADULT&tripType=roundtrip&promoCode=&reset=true&redirectToVision=true&int=HOMEQBOMAIR&leapfrogRequest=true`;
    let urlOneWay = `https://www.southwest.com/air/booking/select.html?originationAirportCode=${depCode}&destinationAirportCode=${arrCode}&returnAirportCode=&departureDate=${depDate}&departureTimeOfDay=ALL_DAY&returnDate=&returnTimeOfDay=ALL_DAY&adultPassengersCount=${paxCount}&seniorPassengersCount=0&fareType=USD&passengerType=ADULT&tripType=oneway&promoCode=&reset=true&redirectToVision=true&int=HOMEQBOMAIR&leapfrogRequest=true`;


    try {
        await page.goto(urlReturn);
        await page.waitForSelector(".air-booking-select-detail")
        const sections = await page.$$(".air-booking-select-detail");

        for(let i = 0; i<sections.length; i++) {
            const section = sections[i];

            const flight = await page.$(".air-booking-select-detail");
            const flightName = await page.evaluate( title => title.innerText, flight);
            console.log("\n\n", flightName);
        }
    
  
    } catch (e) {
        console.log(e);
    }
    

}