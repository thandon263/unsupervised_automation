const puppeteer = require('puppeteer');
const chromium = require("chromium");


module.exports = async function flight_Scrapper() {

    
    const browser = await puppeteer.launch({ headless: false })
    const page = await browser.newPage()

    let depCode = "LAX"; //LAX
    let arrCode = "LAS"; //LAS
    let depDate = "2018-08-27"; //YYYY-MM-DD
    let retDate = "2018-09-4"; //YYYY-MM-DD
    let paxCount = "1"; //1

    let returnTrip = `https://www.google.ca/flights#flt=${depCode}.${arrCode}.${depDate}*${arrCode}.${depCode}.${retDate};c:USD;e:1;sd:1;t:f`
    let oneWayTrip = `https://www.google.ca/flights#flt=${depCode}.${arrCode}.${depDate};c:USD;e:1;sd:1;t:f;tt:o`
    
    try {
        await page.goto(oneWayTrip);
        await page.waitFor(1000);
        await page.waitForSelector(".gws-flights-results__best-flights");

        const sections = await page.$$(".gws-flights-results__result-item");
        console.log(sections.length);

        for (let i = 0; i < 1; i++) {
            const section = sections[i]

            const flight = await page.$(".gws-flights-results__result-item");
            const flightInfo = await page.evaluate(info => info.innerText, flight);

            console.log("\n\n", flightInfo);
        }

        browser.close()
        
    } catch (e) {
        browser.close()
        console.log(e);
    }
    

}
